import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.file.*;

/**
 * Created by Lachlan Stevens on 27/08/2017 at 1:43 PM.
 */
public class Main {

    public static void main(String[] args) {

        try {

            Path file_path   = Paths.get(/*Insert path to input file*/"");
            Path output_path = Paths.get(/*Insert parth to output file (must be .bmp!)*/"");

            File file_out = output_path.toFile();

            if (!file_out.exists()) file_out.createNewFile();

            InputStream input = Files.newInputStream(file_path);
            FileOutputStream output = new FileOutputStream(file_out);

            int byte_count = input.available();

            int size = (int) Math.floor(Math.sqrt(byte_count/3));
            System.out.println("Found Size");

            // Write BMP Header
            byte[] fs = ByteBuffer.allocate(4).putInt(54 + (size^2 * 4)).array();
            byte[] s =  ByteBuffer.allocate(4).putInt(size).array();
            byte[] header = {0x42,  0x4D,   fs[3],  fs[2],  fs[1],  fs[0],  0x0,    0x0,
                             0x0,   0x0,    0x36,   0x0,    0x0,    0x0,    0x28,   0x0,
                             0x0,   0x0,    s[3],   s[2],   s[1],   s[0],   s[3],   s[2],
                             s[1],  s[0],   0x1,    0x0,    0x18,   0x0,    0x0,    0x0,
                             0x0,   0x0,    0x10,   0x0,    0x0,    0x0,    0x13,   0x0B,
                             0x0,   0x0,    0x13,   0x0B,   0x0,    0x0,    0x0,    0x0,
                             0x0,   0x0,    0x0,    0x0,    0x0,    0x0};

            output.write(header);
            System.out.println("Finished Writing Header");

            // Start writing pixel data
            int c = 1;
            while (input.available() > 0){
                if (c % 4 == 0) output.write(new byte[]{0x0});
                else output.write(new byte[]{(byte) input.read()});
                c++;
            }

        } catch (Exception e){
            e.printStackTrace();
        }

    }

}
